import React from "react";
import "bootstrap/dist/css/bootstrap.css";
import Carousel from "react-bootstrap/Carousel";
import {
  FaFacebook,
  FaInstagram,
  FaBehance,
  FaGripLines,
} from "react-icons/fa";
import "./HomePage.css";
export default function HomePage() {
  let check = false;
  let hamberger = () => {
    if (check) {
      check = false;
      document.getElementById("drop-down").style.display = "none";
    } else {
      check = true;
      document.getElementById("drop-down").style.display = "flex";
    }
  };
  return (
    <div style={{ position: "relative" }}>
      <div className="HomePageHeader">
        <div
          style={{
            width: "50%",
            height: "50%",
          }}
        >
          <a href="/">
            <img className="logo" src={require("../img/Logo.jpg")} />
          </a>
        </div>
        <div className="AboutPageNavbar">
          <div>
            <a href="/work" style={{ color: "white", textDecoration: "none" }}>
              WORK
            </a>
          </div>
          <div>
            <a
              href="/gallery"
              style={{ color: "white", textDecoration: "none" }}
            >
              GALLERY
            </a>
          </div>
          <div>
            <a href="/about" style={{ color: "white", textDecoration: "none" }}>
              ABOUT
            </a>
          </div>
          <div>
            <a
              href="https://www.facebook.com/pulu.artandco"
              target="_blank"
              style={{ color: "white" }}
            >
              <FaFacebook />
            </a>
          </div>
          <div>
            <a
              href="https://www.instagram.com/pulu.studio/"
              target="_blank"
              style={{ color: "white" }}
            >
              <FaInstagram />
            </a>
          </div>
          <div>
            <a
              href="https://www.behance.net/pulustudio"
              target="_blank"
              style={{ color: "white" }}
            >
              <FaBehance />
            </a>
          </div>
        </div>
        <div className="navbar-mini">
          <div className="hamburger shadow" id="drop-down">
            <div>
              <a
                href="/work"
                style={{
                  color: "black",
                  textDecoration: "none",
                }}
              >
                WORK
              </a>
            </div>
            <div>
              <a
                href="/gallery"
                style={{
                  color: "black",
                  textDecoration: "none",
                }}
              >
                GALLERY
              </a>
            </div>
            <div>
              <a
                href="/about"
                style={{
                  color: "black",
                  textDecoration: "none",
                }}
              >
                ABOUT
              </a>
            </div>
            <div style={{ display: "flex" }}>
              <div>
                <a
                  href="https://www.facebook.com/pulu.artandco"
                  target="_blank"
                  style={{ color: "black", marginRight: "5px" }}
                >
                  <FaFacebook />
                </a>
              </div>
              <div>
                <a
                  href="https://www.instagram.com/pulu.studio/"
                  target="_blank"
                  style={{ color: "black", marginRight: "5px" }}
                >
                  <FaInstagram />
                </a>
              </div>
              <div>
                <a
                  href="https://www.behance.net/pulustudio"
                  target="_blank"
                  style={{ color: "black" }}
                >
                  <FaBehance />
                </a>
              </div>
            </div>
          </div>
          <div>
            <a onClick={hamberger} style={{ color: "white" }}>
              <FaGripLines />
            </a>
          </div>
        </div>
      </div>
      <div style={{ width: "100vw", height: "100vh", zIndex: "1" }}>
        <Carousel style={{ height: "100vh" }}>
          <Carousel.Item interval={4500}>
            <div
              className="item"
              style={{
                backgroundImage: `url("https://images.squarespace-cdn.com/content/v1/5484b3b4e4b0883e5fe5efa4/1668365333199-3E3TCPTX6PJLRUR6WQP4/20221110+gdd82075.jpg?format=2500w")`,
              }}
            ></div>
          </Carousel.Item>
          <Carousel.Item interval={500}>
            <div
              className="item"
              style={{
                backgroundImage: `url("https://images.squarespace-cdn.com/content/v1/5484b3b4e4b0883e5fe5efa4/1491329459172-H9BJI73XMUXXPHNFZE2K/udon2-webres.jpg?format=2500w")`,
              }}
            ></div>
          </Carousel.Item>
          <Carousel.Item interval={1500}>
            <div
              className="item"
              style={{
                backgroundImage: `url("https://images.squarespace-cdn.com/content/v1/5484b3b4e4b0883e5fe5efa4/1429486990265-6LM4TM7X8SB3ANHGRJUN/adrift-studio-insta.jpg?format=2500w")`,
              }}
            ></div>
          </Carousel.Item>
          <Carousel.Item interval={2500}>
            <div
              className="item"
              style={{
                backgroundImage: `url("https://images.squarespace-cdn.com/content/v1/5484b3b4e4b0883e5fe5efa4/1418069784265-JSFKDW9CSFGXJBBNT9JL/jamesjean-studio-web.jpg?format=2500w")`,
              }}
            ></div>
          </Carousel.Item>
          <Carousel.Item interval={3500}>
            <div
              className="item"
              style={{
                backgroundImage: `url("https://images.squarespace-cdn.com/content/v1/5484b3b4e4b0883e5fe5efa4/1418972959898-3JR5UD7B4I805DPLBLR4/manila-studio-webres.jpg?format=2500w")`,
              }}
            ></div>
          </Carousel.Item>
        </Carousel>
      </div>
    </div>
  );
}

import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { localServ } from "../reduxstore/localService";
import "./LoginPage.css";
export default function LoginPage() {
  let navigate = useNavigate();
  let submit = (e) => {
    e.preventDefault();
    let taikhoan = document.getElementById("taikhoan").value;
    let matkhau = document.getElementById("matkhau").value;
    if (taikhoan == "Puluadmin" && matkhau == "Nduqfu8z@4") {
      localServ.user.set({ taikhoan, matkhau });
      alert("Đăng Nhập Thành Công");
      navigate("/admin");
    } else {
      alert("Sai Tài Khoản Hoặc Mật Khẩu");
      localServ.user.remove();
    }
  };
  return (
    <div className="login">
      <form className="container d-flex flex-column justify-content-center  align-items-center h-100">
        <div style={{ opacity: "0.9" }} className="shadow p-5 bg-body rounded">
          <div className="form-group">
            <label htmlFor="exampleInputEmail1">User Name</label>
            <input
              type="username"
              className="form-control mb-4 mt-2"
              id="taikhoan"
              placeholder="Enter Username"
            />
          </div>
          <div className="form-group">
            <label htmlFor="exampleInputPassword1">Password</label>
            <input
              type="password"
              className="form-control mb-4 mt-2"
              id="matkhau"
              placeholder="Password"
            />
          </div>
          <div className="d-flex justify-content-center">
            <button
              type="submit"
              className="btn btn-primary px-5"
              onClick={submit}
            >
              Login
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}

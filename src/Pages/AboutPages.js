import React from "react";
import emailjs from "@emailjs/browser";
import "./AboutPages.css";

import {
  FaFacebook,
  FaInstagram,
  FaBehance,
  FaGripLines,
} from "react-icons/fa";

export default function AboutPages() {
  function sendEmail(e) {
    e.preventDefault(); //This is important, i'm not sure why, but the email won't send without it

    emailjs
      .sendForm(
        "service_f5z5ghl",
        "template_zctcrh9",
        e.target,
        "tZVNxbUVCJvK2iwbh"
      )
      .then(
        (result) => {
          window.location.reload(); //This is if you still want the page to reload (since e.preventDefault() cancelled that behavior)
        },
        (error) => {
          console.log(error.text);
        }
      );
  }
  let check = false;
  let hamberger = () => {
    if (check) {
      check = false;
      document.getElementById("drop-down").style.display = "none";
    } else {
      check = true;
      document.getElementById("drop-down").style.display = "flex";
    }
  };

  return (
    <div style={{ overflow: "hidden" }}>
      <div style={{ position: "relative" }}>
        <div className="AboutPageHeader  ">
          <div
            style={{
              width: "50%",
              height: "50%",
            }}
          >
            <a href="/">
              <img className="logo" src={require("../img/LogoBlack.jpg")} />
            </a>
          </div>
          <div className="AboutPageNavbar">
            <div>
              <a
                href="/work"
                style={{ color: "black", textDecoration: "none" }}
              >
                WORK
              </a>
            </div>
            <div>
              <a
                href="/gallery"
                style={{ color: "black", textDecoration: "none" }}
              >
                GALLERY
              </a>
            </div>
            <div>
              <a
                href="/about"
                style={{ color: "black", textDecoration: "none" }}
              >
                ABOUT
              </a>
            </div>
            <div>
              <a
                href="https://www.facebook.com/pulu.artandco"
                target="_blank"
                style={{ color: "black" }}
              >
                <FaFacebook />
              </a>
            </div>
            <div>
              <a
                href="https://www.instagram.com/pulu.studio/"
                target="_blank"
                style={{ color: "black" }}
              >
                <FaInstagram />
              </a>
            </div>
            <div>
              <a
                href="https://www.behance.net/pulustudio"
                target="_blank"
                style={{ color: "black" }}
              >
                <FaBehance />
              </a>
            </div>
          </div>
          <div className="navbar-mini">
            <div className="hamburger shadow" id="drop-down">
              <div>
                <a
                  href="/work"
                  style={{
                    color: "black",
                    textDecoration: "none",
                  }}
                >
                  WORK
                </a>
              </div>
              <div>
                <a
                  href="/gallery"
                  style={{
                    color: "black",
                    textDecoration: "none",
                  }}
                >
                  GALLERY
                </a>
              </div>
              <div>
                <a
                  href="/about"
                  style={{
                    color: "black",
                    textDecoration: "none",
                  }}
                >
                  ABOUT
                </a>
              </div>
              <div style={{ display: "flex" }}>
                <div>
                  <a
                    href="https://www.facebook.com/pulu.artandco"
                    target="_blank"
                    style={{ color: "black", marginRight: "5px" }}
                  >
                    <FaFacebook />
                  </a>
                </div>
                <div>
                  <a
                    href="https://www.instagram.com/pulu.studio/"
                    target="_blank"
                    style={{ color: "black", marginRight: "5px" }}
                  >
                    <FaInstagram />
                  </a>
                </div>
                <div>
                  <a
                    href="https://www.behance.net/pulustudio"
                    target="_blank"
                    style={{ color: "black" }}
                  >
                    <FaBehance />
                  </a>
                </div>
              </div>
            </div>
            <div>
              <a onClick={hamberger}>
                <FaGripLines />
              </a>
            </div>
          </div>
        </div>
      </div>
      <div className="container content-email">
        <form className="contact-form box-sendemail" onSubmit={sendEmail}>
          <h2
            className="opa
          "
          >
            CONTACT US
          </h2>
          <div className="content-about">
            <br />
            <br />
            Pulu Studio là công ty thiết kế, sáng tác nghệ thuật và định hướng
            hình ảnh thương hiệu. Mang những tác phẩm nghệ thuật đó đến gần với
            công chúng thông qua các dịch vụ như: thiết kế các sản phẩm bao bì
            của doanh nghiệp, thiết kế ấn phẩm truyền thông quảng cáo, v.v…
            <br />
            <br />
            Đây là nơi chúng tôi thi nhau kể những câu chuyện của mọi người và
            của chính mình theo cách riêng dưới góc nhìn mỹ thuật một cách sâu
            sắc và phấn khích nhất!
            <br />
            <br />
            <b>Address: 205/24 No8 Street, Go Vap District, Ho Chi Minh City</b>
            <br />
            <b>Phone: (+84) 90 4233 835</b>
            <br />
            <br />
            Please complete the form below to email us!
          </div>
          <label>Email *</label>
          <input className="mb-3 inp" type="email" name="from_email" />
          <label>Message *</label>
          <textarea className="mb-3 inp" name="message" />
          <input className="submit-button" type="submit" value="SUBMIT" />
        </form>
      </div>
    </div>
  );
}

import React, { useEffect } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { localServ } from "../reduxstore/localService";
import "./AdminPages.css";
export default function AdminPages() {
  let workpicture = [];
  let gallerypicture = [];
  let isLogin = localServ.user.get();
  let navigate = useNavigate();
  let logout = () => {
    localServ.user.remove();
    alert("Đăng Xuất Thành Công");
    navigate("/login");
  };
  async function getworkpicture() {
    let data = await axios.get(
      "https://63e3488365ae4931770be57e.mockapi.io/Work"
    );
    workpicture = data.data;
    workpicture.sort((a, b) => {
      const projectA = a.project.toUpperCase(); // ignore upper and lowercase
      const projectB = b.project.toUpperCase(); // ignore upper and lowercase
      if (projectA < projectB) {
        return -1;
      }
      if (projectA > projectB) {
        return 1;
      }
      return 0;
    });
    loadpictureleft();
  }

  async function getgallerypicture() {
    let data = await axios.get(
      "https://63e3488365ae4931770be57e.mockapi.io/Gallery"
    );
    gallerypicture = data.data;
    gallerypicture.sort((a, b) => {
      const projectA = a.project.toUpperCase(); // ignore upper and lowercase
      const projectB = b.project.toUpperCase(); // ignore upper and lowercase
      if (projectA < projectB) {
        return -1;
      }
      if (projectA > projectB) {
        return 1;
      }
      return 0;
    });
    loadpictureright();
  }
  let loadpictureleft = () => {
    let content = "";
    for (let e of workpicture) {
      content += `<div> <a href=${e.link} target="_blank">${e.tenhinh}</a></div> `;
    }
    document.getElementById("left").innerHTML += content;
  };
  let loadpictureright = () => {
    let content = "";
    for (let e of gallerypicture) {
      content += `<div> <a href=${e.link} target="_blank">${e.tenhinh}</a></div> `;
    }
    document.getElementById("right").innerHTML += content;
  };

  useEffect(() => {
    if (!isLogin) {
      navigate("/login");
    } else {
      getworkpicture();
      getgallerypicture();
    }
  }, [workpicture, gallerypicture]);

  let back = () => {
    document.getElementById("addpicture").style.display = "none";
    document.getElementById("editpicture").style.display = "none";
    document.getElementById("deletepicture").style.display = "none";
  };
  let addpicture = () => {
    document.getElementById("addpicture").style.display = "flex";
    document.getElementById("work").innerHTML =
      "Work " + workpicture.length + "/100";
    document.getElementById("gallery").innerHTML =
      "Gallery " + gallerypicture.length + "/100";
  };
  async function addpicturesuccess(e) {
    e.preventDefault();
    let project = document.getElementById("addproject").value;
    let tenhinh = document.getElementById("addtenhinh").value;
    let mota1 = document.getElementById("addmota1").value;
    let mota2 = document.getElementById("addmota2").value;
    let mota3 = document.getElementById("addmota3").value;
    let mota =
      `<div class="mota1">` +
      mota1 +
      `</div>` +
      `<div class="mota2">` +
      mota2 +
      `</div>` +
      `<div class="mota3">` +
      mota3 +
      `</div>`;
    let link = document.getElementById("addlink").value;
    let check = document.querySelector('input[name="addcheck"]:checked').value;
    if (check == "work") {
      await axios.post("https://63e3488365ae4931770be57e.mockapi.io/Work", {
        tenhinh,
        mota,
        link,
        project,
      });
      alert("Thêm Hình Thành Công");
    } else if (check == "gallery") {
      await axios.post("https://63e3488365ae4931770be57e.mockapi.io/Gallery", {
        tenhinh,
        mota,
        link,
        project,
      });
      alert("Thêm Hình Thành Công");
    }
    document.getElementById("addpicture").style.display = "none";
  }

  let editpicture = () => {
    document.getElementById("editpicture").style.display = "flex";
  };
  async function editpicturesuccess(e) {
    e.preventDefault();
    let tenhinh = document.getElementById("edittenhinh").value;
    let mota1 = document.getElementById("editmota1").value;
    let mota2 = document.getElementById("editmota2").value;
    let mota3 = document.getElementById("editmota3").value;
    let mota =
      `<div class="mota1">` +
      mota1 +
      `</div>` +
      `<div class="mota2">` +
      mota2 +
      `</div>` +
      `<div class="mota3">` +
      mota3 +
      `</div>`;
    let link = document.getElementById("editlink").value;
    let check = document.querySelector('input[name="editcheck"]:checked').value;

    if (check == "work") {
      let element = workpicture.find((element) => element.tenhinh == tenhinh);
      if (element) {
        await axios.put(
          "https://63e3488365ae4931770be57e.mockapi.io/Work/" + element.id,
          { tenhinh, mota, link }
        );
        alert("Sửa Hình Thành Công");
      } else {
        alert("Không Tìm Thấy Hình Để Sửa");
      }
    } else if (check == "gallery") {
      let element = gallerypicture.find(
        (element) => element.tenhinh == tenhinh
      );
      if (element) {
        await axios.put(
          "https://63e3488365ae4931770be57e.mockapi.io/Gallery/" + element.id,
          { tenhinh, mota, link }
        );
        alert("Sửa Hình Thành Công");
      } else {
        alert("Không Tìm Thấy Hình Để Sửa");
      }
    } else {
      alert("Fail");
    }
    document.getElementById("editpicture").style.display = "none";
  }
  let deletepicture = () => {
    document.getElementById("deletepicture").style.display = "flex";
  };

  async function deletepicturesuccess(e) {
    e.preventDefault();
    let tenhinh = document.getElementById("deletetenhinh").value;
    let check = document.querySelector(
      'input[name="deletecheck"]:checked'
    ).value;
    if (check == "work") {
      let element = workpicture.find((element) => element.tenhinh == tenhinh);
      if (element) {
        await axios.delete(
          "https://63e3488365ae4931770be57e.mockapi.io/Work/" + element.id
        );
        alert("Xoá Hình Thành Công");
      } else {
        alert("Không Tìm Thấy Hình");
      }
    } else if (check == "gallery") {
      let element = gallerypicture.find(
        (element) => element.tenhinh == tenhinh
      );
      if (element) {
        await axios.delete(
          "https://63e3488365ae4931770be57e.mockapi.io/Gallery/" + element.id
        );
        alert("Xoá Hình Thành Công");
      } else {
        alert("Không Tìm Thấy Hình");
      }
    }
    document.getElementById("deletepicture").style.display = "none";
  }

  async function checkname(e) {
    e.preventDefault();
    let tenhinh = document.getElementById("edittenhinh").value;
    console.log("tenhinh:", tenhinh);
    let check = document.querySelector('input[name="editcheck"]:checked').value;

    if (check == "work") {
      let data = await axios.get(
        "https://63e3488365ae4931770be57e.mockapi.io/Work"
      );
      let workpicture = data.data;
      let element = workpicture.find((element) => element.tenhinh == tenhinh);
      if (element) {
        console.log(element);
        let mota = element.mota;
        let chuoi = mota.split("</div>");
        let mota1 = chuoi[0].split('<div class="mota1">');
        let mota2 = chuoi[1].split('<div class="mota2">');
        let mota3 = chuoi[2].split('<div class="mota3">');
        document.getElementById("editproject").value = element.project;
        document.getElementById("editmota1").value = mota1[1];
        document.getElementById("editmota2").value = mota2[1];
        document.getElementById("editmota3").value = mota3[1];
        document.getElementById("editlink").value = element.link;
      } else {
        alert("Không Tìm Thấy Hình Để Sửa");
      }
    } else if (check == "gallery") {
      let data = await axios.get(
        "https://63e3488365ae4931770be57e.mockapi.io/Gallery"
      );
      let gallerypicture = data.data;
      let element = gallerypicture.find(
        (element) => element.tenhinh == tenhinh
      );
      if (element) {
        let mota = element.mota;
        let chuoi = mota.split("</div>");
        let mota1 = chuoi[0].split('<div class="mota1">');
        let mota2 = chuoi[1].split('<div class="mota2">');
        let mota3 = chuoi[2].split('<div class="mota3">');
        document.getElementById("editproject").value = element.project;
        document.getElementById("editmota1").value = mota1[1];
        document.getElementById("editmota2").value = mota2[1];
        document.getElementById("editmota3").value = mota3[1];
        document.getElementById("editlink").value = element.link;
      } else {
        alert("Không Tìm Thấy Hình Để Sửa");
      }
    } else {
      alert("Fail");
    }
  }
  let note = "<br/>";
  return (
    <div style={{ position: "relative" }}>
      <div id="addpicture">
        <div className=" p-3 mb-5 bg-white rounded">
          <h2>Add Picture (Thêm "_thumbnail" cho project đại diện)</h2>
          <form style={{ width: "50vw" }}>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Project :</label>
              <input className="form-control" id="addproject" />
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Tên Hình :</label>
              <input className="form-control" id="addtenhinh" />
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">
                Mô Tả 1 : (thêm "{note}" để xuống dòng)
              </label>
              <input className="form-control" id="addmota1" />
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Mô Tả 2 :</label>
              <input className="form-control" id="addmota2" />
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Mô Tả 3 :</label>
              <input className="form-control" id="addmota3" />
            </div>

            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Link :</label>
              <input className="form-control" id="addlink" />
            </div>
            <form id="addcheck">
              <input
                className="form-check-input"
                type="radio"
                name="addcheck"
                value="work"
                defaultChecked
              />
              <span id="work"></span>
              <br />
              <input
                className="form-check-input"
                type="radio"
                name="addcheck"
                value="gallery"
              />
              <span id="gallery"></span>
            </form>

            <button
              type="submit"
              className="btn btn-primary"
              onClick={addpicturesuccess}
            >
              Add Picture
            </button>
            <button type="submit" className="btn btn-danger" onClick={back}>
              Back
            </button>
          </form>
        </div>
      </div>
      <div id="editpicture">
        <div className="shadow p-3 mb-5 bg-white rounded">
          <form style={{ width: "50vw" }}>
            <h2>Edit Picture (Edit Theo Tên Hình)</h2>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Project :</label>
              <input className="form-control" id="editproject" />
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Tên Hình :</label>
              <input className="form-control" id="edittenhinh" />
            </div>

            <div className="form-group">
              <label htmlFor="exampleInputEmail1">
                Mô Tả 1 : (thêm "{note}" để xuống dòng)
              </label>
              <input className="form-control" id="editmota1" />
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Mô Tả 2 :</label>
              <input className="form-control" id="editmota2" />
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Mô Tả 3 :</label>
              <input className="form-control" id="editmota3" />
            </div>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Link :</label>
              <input className="form-control" id="editlink" />
            </div>
            <div>
              <form id="editcheck">
                <input
                  className="form-check-input"
                  type="radio"
                  value="work"
                  name="editcheck"
                  defaultChecked
                />
                Work
                <br />
                <input
                  className="form-check-input"
                  type="radio"
                  name="editcheck"
                  value="gallery"
                />
                Gallery
              </form>
            </div>
            <button className="btn btn-success" onClick={checkname}>
              Check
            </button>
            <button
              type="submit"
              className="btn btn-primary"
              onClick={editpicturesuccess}
            >
              Edit Picture
            </button>
            <button type="submit" className="btn btn-danger" onClick={back}>
              Back
            </button>
          </form>
        </div>
      </div>
      <div id="deletepicture">
        <div className="shadow p-3 mb-5 bg-white rounded">
          <form style={{ width: "50vw" }}>
            <h2>Delete Picture</h2>
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Tên Hình :</label>
              <input className="form-control" id="deletetenhinh" />
            </div>
            <div>
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  name="deletecheck"
                  value="work"
                  defaultChecked
                />
                Work
              </div>
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  value="gallery"
                  name="deletecheck"
                />
                Gallery
              </div>
            </div>

            <button
              type="submit"
              className="btn btn-primary"
              onClick={deletepicturesuccess}
            >
              Delete Picture
            </button>
            <button type="submit" className="btn btn-danger" onClick={back}>
              Back
            </button>
          </form>
        </div>
      </div>
      <div className="container mt-3">
        <div className="d-flex justify-content-between mb-3">
          <ul className="nav">
            <li className="nav-item">
              <button class="btn" type="submit" onClick={addpicture}>
                Add Picture
              </button>
            </li>
            <li className="nav-item">
              <button className="btn" type="submit" onClick={editpicture}>
                Edit Picture
              </button>
            </li>
            <li className="nav-item">
              <button className="btn" type="submit" onClick={deletepicture}>
                Delete Picture
              </button>
            </li>
          </ul>
          <div>
            <button className="btn btn-danger" type="submit" onClick={logout}>
              Logout
            </button>
          </div>
        </div>
        <div>
          <div className="container">
            <div className="list-picture">
              <div id="left">
                <h2>Work Picture</h2>
              </div>
              <div id="right">
                <h2>Gallery Picture</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

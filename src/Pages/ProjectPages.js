import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import "./ProjectPages.css";
export default function ProjectPages() {
  let { tenproject } = useParams();
  let workpicture = [];
  let gallerypicture = [];
  let datapicture = [];
  async function showpicture() {
    let data1 = await axios.get(
      "https://63e3488365ae4931770be57e.mockapi.io/Gallery"
    );
    gallerypicture = data1.data;
    gallerypicture.sort((a, b) => {
      const projectA = a.project.toUpperCase(); // ignore upper and lowercase
      const projectB = b.project.toUpperCase(); // ignore upper and lowercase
      if (projectA < projectB) {
        return -1;
      }
      if (projectA > projectB) {
        return 1;
      }
      return 0;
    });

    let data = await axios.get(
      "https://63e3488365ae4931770be57e.mockapi.io/Work"
    );
    workpicture = data.data;
    workpicture.sort((a, b) => {
      const projectA = a.project.toUpperCase(); // ignore upper and lowercase
      const projectB = b.project.toUpperCase(); // ignore upper and lowercase
      if (projectA < projectB) {
        return -1;
      }
      if (projectA > projectB) {
        return 1;
      }
      return 0;
    });
    datapicture.push(...workpicture);
    datapicture.push(...gallerypicture);
    let content = "";
    for (let e of datapicture) {
      if (e.project == tenproject) {
        content += `<div class="container">
        <img
        class="data-picture"
        src=${e.link}
        />
        <p class="container" style="color:white" >${e.mota}</p>
      </div>
        `;
      }
    }
    document.getElementById("content").innerHTML = content;
  }

  useEffect(() => {
    showpicture();
  }, [workpicture, gallerypicture]);
  return <div id="content" style={{ backgroundColor: "black" }}></div>;
}

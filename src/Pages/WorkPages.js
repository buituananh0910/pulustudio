import React, { useEffect } from "react";
import axios from "axios";
import "./WorkPages.css";
import "bootstrap/dist/css/bootstrap.css";
import "react-multi-carousel/lib/styles.css";
import {
  FaFacebook,
  FaInstagram,
  FaBehance,
  FaGripLines,
} from "react-icons/fa";
export default function WorkPages() {
  let check = false;
  let hamberger = () => {
    if (check) {
      check = false;
      document.getElementById("drop-down").style.display = "none";
    } else {
      check = true;
      document.getElementById("drop-down").style.display = "flex";
    }
  };

  let workpicture = [];
  let showworkpicture = (workpicture) => {
    let content = "";
    for (let e of workpicture) {
      if (e.project.includes("_thumbnail")) {
        let nameproject = e.project.replace("_thumbnail", "");
        content += `
      <a href="/project/${nameproject}" class="project" >
        <div style="position:relative;" >
          <p class="workprojectname" >
            ${nameproject}
          </p>
          <img src=${e.link} class="WorkProjectItem"/>
        </div>
      </a>`;
      }
    }
    document.getElementById("WorkPageShowProject").innerHTML = content;
  };
  async function getworkpicture() {
    let data = await axios.get(
      "https://63e3488365ae4931770be57e.mockapi.io/Work"
    );
    workpicture = data.data;
    workpicture.sort((a, b) => {
      const projectA = a.project.toUpperCase(); // ignore upper and lowercase
      const projectB = b.project.toUpperCase(); // ignore upper and lowercase
      if (projectA < projectB) {
        return -1;
      }
      if (projectA > projectB) {
        return 1;
      }
      return 0;
    });
    console.log(workpicture);
    showworkpicture(workpicture);
  }
  useEffect(() => {
    getworkpicture();
  }, [workpicture]);
  return (
    <div style={{ position: "relative" }}>
      <div className="WorkPageHeader ">
        <div
          style={{
            width: "50%",
            height: "50%",
          }}
        >
          <a href="/">
            <img className="logo" src={require("../img/LogoBlack.jpg")} />
          </a>
        </div>
        <div className="AboutPageNavbar">
          <div>
            <a href="/work" style={{ color: "black", textDecoration: "none" }}>
              WORK
            </a>
          </div>
          <div>
            <a
              href="/gallery"
              style={{ color: "black", textDecoration: "none" }}
            >
              GALLERY
            </a>
          </div>
          <div>
            <a href="/about" style={{ color: "black", textDecoration: "none" }}>
              ABOUT
            </a>
          </div>
          <div>
            <a
              href="https://www.facebook.com/pulu.artandco"
              target="_blank"
              style={{ color: "black" }}
            >
              <FaFacebook />
            </a>
          </div>
          <div>
            <a
              href="https://www.instagram.com/pulu.studio/"
              target="_blank"
              style={{ color: "black" }}
            >
              <FaInstagram />
            </a>
          </div>
          <div>
            <a
              href="https://www.behance.net/pulustudio"
              target="_blank"
              style={{ color: "black" }}
            >
              <FaBehance />
            </a>
          </div>
        </div>
        <div className="navbar-mini">
          <div className="hamburger shadow" id="drop-down">
            <div>
              <a
                href="/work"
                style={{
                  color: "black",
                  textDecoration: "none",
                }}
              >
                WORK
              </a>
            </div>
            <div>
              <a
                href="/gallery"
                style={{
                  color: "black",
                  textDecoration: "none",
                }}
              >
                GALLERY
              </a>
            </div>
            <div>
              <a
                href="/about"
                style={{
                  color: "black",
                  textDecoration: "none",
                }}
              >
                ABOUT
              </a>
            </div>
            <div style={{ display: "flex" }}>
              <div>
                <a
                  href="https://www.facebook.com/pulu.artandco"
                  target="_blank"
                  style={{ color: "black", marginRight: "5px" }}
                >
                  <FaFacebook />
                </a>
              </div>
              <div>
                <a
                  href="https://www.instagram.com/pulu.studio/"
                  target="_blank"
                  style={{ color: "black", marginRight: "5px" }}
                >
                  <FaInstagram />
                </a>
              </div>
              <div>
                <a
                  href="https://www.behance.net/pulustudio"
                  target="_blank"
                  style={{ color: "black" }}
                >
                  <FaBehance />
                </a>
              </div>
            </div>
          </div>
          <div>
            <a onClick={hamberger}>
              <FaGripLines />
            </a>
          </div>
        </div>
      </div>
      <div className="work-show-project">
        <div className="WorkPageShowProject" id="WorkPageShowProject"></div>
      </div>
    </div>
  );
}

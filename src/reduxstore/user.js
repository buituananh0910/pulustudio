import { createSlice } from "@reduxjs/toolkit";
import { localServ } from "./localService";
export const userSlice = createSlice({
  name: "counter",
  initialState: {
    userInfor: localServ.user.get(),
  },
  reducers: {},
});

export const {} = userSlice.actions;

export default userSlice.reducer;

import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import HomePage from "./Pages/HomePage";
import LoginPage from "./Pages/LoginPage";
import AdminPages from "./Pages/AdminPages";
import WorkPages from "./Pages/WorkPages";
import GalleryPages from "./Pages/GalleryPages";
import ProjectPages from "./Pages/ProjectPages";
import AboutPages from "./Pages/AboutPages";
function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/admin" element={<AdminPages />} />
          <Route path="/work" element={<WorkPages />} />
          <Route path="/gallery" element={<GalleryPages />} />
          <Route path="/project/:tenproject" element={<ProjectPages />} />
          <Route path="/about" element={<AboutPages />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;

import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import { createStore, compose, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import App from "./App";
import store from "./reduxstore/store.js";
import "react-bootstrap-carousel/dist/react-bootstrap-carousel.css";
import "bootstrap/dist/css/bootstrap.min.css";
const root = ReactDOM.createRoot(document.getElementById("root"));
// let store = createStore(rootReducer);
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);
